$(function() {
  // header
  var headers = document.querySelectorAll ('*[data-controller="header"]');

  var headerLength = headers.length;
  for (var i = 0; i < headerLength; i++) {
    //Select all relevant Header-Items
    var header = headers[i];
    var burgerMenu = header.querySelector ('*[data-controller="header/burgermenu"]');
    var navigation = header.querySelector ('*[data-controller="header/navigation"]');

    //Do nothing, if required elements does not exist
    if (!burgerMenu || !navigation) {
      return;
    }

    //Init Tiny-Slider
    var sliderMainNav = null;
    var sliderSecNav = null;
    var defaultSliderSettings = {
      slideBy: 3,
      autoWidth: true,
      useLocalStorage: false
    };

    var sliderMainNavButtonPrevious = header.querySelector ("*[data-toggle=\"header/navigation/menu/main/slider/prev\"]");
    var sliderMainNavButtonNext = header.querySelector ("*[data-toggle=\"header/navigation/menu/main/slider/next\"]");
    var sliderSubNavButtonPrevious = header.querySelector ("*[data-toggle=\"header/navigation/menu/sub/slider/prev\"]");
    var sliderSubNavButtonNext = header.querySelector ("*[data-toggle=\"header/navigation/menu/sub/slider/next\"]");
    var hasSubNavElement = false; // !!header.querySelector (".header-subnavigation-list:not(.d-none)");

    //Header-Slider-Config
    var initSliders = function() {
      if (closeMobileMenuBreakpoint > window.innerWidth) {
        return;
      }

      //Initialize Slider
      if (sliderMainNav === null) {
        var sliderMainNavList = header.querySelector ("*[data-identifier=\"header/navigation/main\"]");
        sliderMainNav = new themeSlider (
          defaultSliderSettings,
          false,
          navigation,
          sliderMainNavList,
          sliderMainNavButtonPrevious,
          sliderMainNavButtonNext
        );
        sliderMainNav.initialize ();
      }

      if (hasSubNavElement && sliderSecNav === null) {
        const sliderSecNavListEl = header.querySelector (".header-subnavigation-list:not(.d-none)");
        sliderSecNav = new themeSlider (
          defaultSliderSettings,
          false,
          navigation,
          sliderSecNavListEl,
          sliderSubNavButtonPrevious,
          sliderSubNavButtonNext
        );
        sliderSecNav.initialize ();
      }
    }
    var deconstructSliders = () => {
      if (sliderMainNav) {
        sliderMainNav.terminate ();
        sliderMainNav = null;
      }
      if (sliderSecNav) {
        sliderSecNav.terminate ();
        sliderSecNav = null;
      }
    }
    initSliders ();


    //Method to dispatch event
    var toggleMobileMenu = (opened, breakpointChange = false) => {
      //Show or Hide the Buttons and Menus
      if (opened) {
        header.classList.add ("header-mobile-overlay");
        burgerMenu.classList.add ('is-active');

        deconstructSliders ();
      } else {
        header.classList.remove ("header-mobile-overlay");
        burgerMenu.classList.remove ('is-active');
      }

      // Dispatch/Trigger/Fire the event (before the change)
      header.dispatchEvent(new CustomEvent("open", {
        detail: {
          opened,
          "breakpoint-has-changed": breakpointChange,
        },
      }));

      if (!opened) {
        initSliders ();
      }

      //Set Aria-Attributes for Accessibility
      burgerMenu.setAttribute ('aria-pressed', opened);
      navigation.setAttribute ('aria-expanded', opened);
    };

    //Add Click-Listener to the BurgerMenu-Button
    burgerMenu.addEventListener ('click', function(event) {
      event.preventDefault ();
      toggleMobileMenu (burgerMenu.classList.contains ('is-active') === false);
    });

    //Add Event-Listener to close mobile-menu on resize, when on breakpoint >md
    window.resizeThrottled (() => {
      if (closeMobileMenuBreakpoint <= window.innerWidth) {
        toggleMobileMenu (false, true);
      }
    }, 200);
  }

})
